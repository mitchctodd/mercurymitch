package com.mitchtodd.mercurymitch.util;

import com.mitchtodd.mercurymitch.data.api.github.GitHubService;
import com.mitchtodd.mercurymitch.data.api.movies.MovieService;

import retrofit.RestAdapter;

/**
 * Utils for setting up API service implementations.
 */
public final class ApiUtil {
    public static GitHubService createGitHubService() {
        return new RestAdapter.Builder()
                .setEndpoint(GitHubService.BASE_URL)
                .build().create(GitHubService.class);
    }

    public static MovieService createMovieService() {
        return new RestAdapter.Builder()
                .setEndpoint(MovieService.BASE_URL)
                .build().create(MovieService.class);
    }
}
