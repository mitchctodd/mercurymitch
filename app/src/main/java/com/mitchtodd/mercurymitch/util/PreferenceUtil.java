package com.mitchtodd.mercurymitch.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

/**
 * Utils for saving and loading POJOs to and from SharedPreferences.
 */
public class PreferenceUtil {

    /**
     * Load the an object from SharedPreferences.
     *
     * @param context
     * @param key
     */
    public static <T> T loadFromJson(Context context, String key, Class<T> type) {
        T object = null;
        String json = PreferenceManager.getDefaultSharedPreferences(context).getString(key, null);
        if (json != null) {
            try {
                object = new GsonBuilder().create().fromJson(json, type);
            } catch (JsonSyntaxException e) {
                //Log.e("Error loading object: " + type, e);
                clear(context, key);
            }
        }
        return object;
    }

    /**
     * Serialize the object to JSON and save to SharedPreferences using the key.
     *
     * @param context
     * @param key
     * @param object  to save
     */
    public static void saveAsJson(Context context, String key, Object object) {
        String json = new GsonBuilder().create().toJson(object);
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(key, json).commit();
    }

    /**
     * Clear the passed key from SharedPreferences.
     *
     * @param context
     * @param key
     */
    public static void clear(Context context, String key) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().remove(key).commit();
    }

    public static boolean exists(Context context, String key) {
        return PreferenceManager.getDefaultSharedPreferences(context).contains(key);
    }

    public static boolean loadBoolean(Context context, String key, boolean defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(key, defaultValue);
    }

    public static void saveBoolean(Context context, String key, boolean value) {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(context).edit();
        edit.putBoolean(key, value);
        edit.apply();
    }

    public static int loadInt(Context context, String key, int defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(key, defaultValue);
    }

    public static void saveInt(Context context, String key, int value) {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(context).edit();
        edit.putInt(key, value);
        edit.apply();
    }
}

