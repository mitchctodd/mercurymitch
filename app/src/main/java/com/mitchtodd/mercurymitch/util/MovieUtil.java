package com.mitchtodd.mercurymitch.util;

import com.mitchtodd.mercurymitch.data.api.github.model.RepoCommit;

import java.util.List;

/**
 * Utils for working with {@link com.mitchtodd.mercurymitch.data.api.movies.model.Movie} data.
 */
public final class MovieUtil {
    public static String getLatestCommitSha(List<RepoCommit> repoCommits) {
        String latestCommit = "";
        long latest = 0;
        for(RepoCommit commit : repoCommits) {
            if(commit.getCommit().getCommitter().getDate().getTime() > latest) {
                latestCommit = commit.getSha();
                latest = commit.getCommit().getCommitter().getDate().getTime();
            }
        }
        return latestCommit;
    }
}
