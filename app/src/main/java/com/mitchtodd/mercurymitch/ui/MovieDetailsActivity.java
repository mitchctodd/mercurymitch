package com.mitchtodd.mercurymitch.ui;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mitchtodd.mercurymitch.R;
import com.mitchtodd.mercurymitch.util.ApiUtil;
import com.mitchtodd.mercurymitch.data.api.movies.model.Movie;
import com.mitchtodd.mercurymitch.data.api.movies.model.MovieDetails;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Takes in a commit id and movie id and displays movie details. If we've already retrieve movie
 * details within an app session, we simply update the view. Otherwise, we load the details from
 * the MovieService.
 */
public class MovieDetailsActivity extends AppCompatActivity {
    public final static String EXTRA_COMMIT_ID = "EXTRA_COMMIT_ID";
    public final static String EXTRA_MOVIE_ID = "EXTRA_MOVIE_ID";

    @InjectView(R.id.movie_details_layout)
    FrameLayout mMovieDetailsLayout;
    @InjectView(R.id.loading_progress)
    ProgressBar mLoadingProgressBar;
    @InjectView(R.id.no_connection_layout)
    LinearLayout mNoConnectionLayout;
    @InjectView(R.id.movie_detail_title_text)
    TextView mTitleTextView;
    @InjectView(R.id.movie_detail_subtitle_text)
    TextView mSubTitleTextView;
    @InjectView(R.id.movie_detail_poster_image)
    ImageView mPosterImageView;
    @InjectView(R.id.movie_detail_plot_text)
    TextView mPlotTextView;
    @InjectView(R.id.movie_detail_rating_text)
    TextView mRatingTextView;
    @InjectView(R.id.movie_detail_metascore_text)
    TextView mMetascoreTextView;
    @InjectView(R.id.movie_detail_awards_text)
    TextView mAwardsTextView;
    @InjectView(R.id.movie_detail_director_text)
    TextView mDirectorTextView;
    @InjectView(R.id.movie_detail_writers_text)
    TextView mWritersTextView;
    @InjectView(R.id.movie_detail_actors_text)
    TextView mActorsTextView;
    @InjectView(R.id.movie_detail_released_text)
    TextView mReleasedTextView;
    @InjectView(R.id.movie_detail_country_text)
    TextView mCountryTextView;
    @InjectView(R.id.movie_detail_languages_text)
    TextView mLanguagesTextView;

    private Handler mHandler = new Handler();

    private MovieDetailsViewState mCurrentViewState;

    private String mCommitId;
    private Movie mMovie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
        ButterKnife.inject(this);

        mCommitId = getIntent().getStringExtra(EXTRA_COMMIT_ID);
        mMovie = (Movie) getIntent().getSerializableExtra(EXTRA_MOVIE_ID);

        if(mMovie.getMovieDetails() == null) {
            updateData();
        } else {
            updateView();
        }
    }

    /**
     * Load MovieDetails from MovieService.
     */
    private void updateData() {
        setViewState(MovieDetailsViewState.LOADING);

        ApiUtil.createMovieService().getMovieDetails(mCommitId, mMovie.getImdbId(),
                new Callback<MovieDetails>() {
                    @Override
                    public void success(MovieDetails movieDetails, Response response) {
                        mMovie.setMovieDetails(movieDetails);
                        updateView();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        handleServiceError();
                    }
                });
    }

    private void handleServiceError() {
        setViewState(MovieDetailsViewState.NO_CONNECTION);
    }

    @OnClick(R.id.no_connection_retry_button)
    public void retry() {
        updateData();
    }

    private void updateView() {
        setViewState(MovieDetailsViewState.SHOW_MOVIE_DETAILS);

        mTitleTextView.setText(String.format("%s (%d)", mMovie.getTitle(), mMovie.getYear()));
        mSubTitleTextView.setText(String.format("%s | %s | %s", mMovie.getMovieDetails().getRated(),
                mMovie.getMovieDetails().getRuntime(), TextUtils.join(", ", mMovie.getMovieDetails().getGenre())));

        Picasso.with(this).load(mMovie.getPoster())
                .placeholder(R.drawable.ic_local_movies_grey600_48dp).into(mPosterImageView);
        mPlotTextView.setText(mMovie.getMovieDetails().getPlot());

        String votesNum = NumberFormat.getNumberInstance(Locale.US).format(mMovie.getImdbVotes());
        mRatingTextView.setText(String.format("%.1f/10 (%s %s)", mMovie.getImdbRating(),
                votesNum, getString(R.string.votes)));
        mMetascoreTextView.setText(mMovie.getMovieDetails().getMetascore());
        mAwardsTextView.setText(mMovie.getMovieDetails().getAwards());

        mDirectorTextView.setText(mMovie.getMovieDetails().getDirector());
        mWritersTextView.setText(mMovie.getMovieDetails().getWriter());
        mActorsTextView.setText(TextUtils.join(", ", mMovie.getMovieDetails().getActors()));

        mReleasedTextView.setText(mMovie.getMovieDetails().getReleased());
        mCountryTextView.setText(mMovie.getMovieDetails().getCountry());
        mLanguagesTextView.setText(TextUtils.join(", ", mMovie.getMovieDetails().getLanguage()));
    }

    /**
     * Update visibility of views depending on current state
     *
     * @param state
     */
    private void setViewState(MovieDetailsViewState state) {
        mCurrentViewState = state;

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                switch(mCurrentViewState) {
                    case LOADING:
                        mMovieDetailsLayout.setVisibility(View.INVISIBLE);
                        mNoConnectionLayout.setVisibility(View.INVISIBLE);
                        mLoadingProgressBar.setVisibility(View.VISIBLE);
                        break;
                    case NO_CONNECTION:
                        mMovieDetailsLayout.setVisibility(View.INVISIBLE);
                        mNoConnectionLayout.setVisibility(View.VISIBLE);
                        mLoadingProgressBar.setVisibility(View.INVISIBLE);
                        break;
                    case SHOW_MOVIE_DETAILS:
                        mMovieDetailsLayout.setVisibility(View.VISIBLE);
                        mNoConnectionLayout.setVisibility(View.INVISIBLE);
                        mLoadingProgressBar.setVisibility(View.INVISIBLE);
                        break;
                }
            }
        });
    }

    private enum MovieDetailsViewState {
        LOADING,
        NO_CONNECTION,
        SHOW_MOVIE_DETAILS
    }
}
