package com.mitchtodd.mercurymitch.ui;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mitchtodd.mercurymitch.R;
import com.mitchtodd.mercurymitch.data.MoviesModel;
import com.mitchtodd.mercurymitch.data.api.movies.model.Movie;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by mitch on 3/12/16.
 */
public class MovieListAdapter extends RecyclerView.Adapter<MovieListAdapter.ViewHolder> {
    private Context mContext;

    private MoviesModel mMoviesModel;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View rootView;
        public TextView titleTextView;
        public TextView yearTextView;
        public TextView ratingTextView;
        public TextView rankTextView;
        public ImageView posterImageView;
        public ImageView imdbLinkImageView;

        public ViewHolder(View itemView) {
            super(itemView);

            rootView = itemView;
            titleTextView = (TextView) itemView.findViewById(R.id.title_text_view);
            yearTextView = (TextView) itemView.findViewById(R.id.year_text_view);
            ratingTextView = (TextView) itemView.findViewById(R.id.rating_text_view);
            rankTextView = (TextView) itemView.findViewById(R.id.rank_text_view);
            posterImageView = (ImageView) itemView.findViewById(R.id.poster_image_view);
            imdbLinkImageView = (ImageView) itemView.findViewById(R.id.imdb_image_view);
        }
    }

    public MovieListAdapter(MoviesModel moviesModel) {
        mMoviesModel = moviesModel;
    }

    public void setMoviesModel(MoviesModel moviesModel) {
        mMoviesModel = moviesModel;
    }

    @Override
    public MovieListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_list_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Movie movie = mMoviesModel.getMovies().get(position);

        holder.titleTextView.setText(movie.getTitle());
        holder.yearTextView.setText(String.format("%d", movie.getYear()));
        String votesNum = NumberFormat.getNumberInstance(Locale.US).format(movie.getImdbVotes());
        holder.ratingTextView.setText(String.format("%.1f (%s %s)", movie.getImdbRating(),
                votesNum, mContext.getString(R.string.votes)));
        holder.rankTextView.setText(String.format("%d", movie.getRank()));

        Picasso.with(mContext).load(movie.getPoster())
                .placeholder(R.drawable.ic_local_movies_grey600_48dp).into(holder.posterImageView);

        holder.rootView.setTag(movie);
        holder.rootView.setOnClickListener(mOnMovieClicked);

        holder.imdbLinkImageView.setTag(movie);
        holder.imdbLinkImageView.setOnClickListener(mOnMovieImdbLinkClicked);
    }

    @Override
    public int getItemCount() {
        return mMoviesModel.getMovies().size();
    }

    private View.OnClickListener mOnMovieClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Movie movie = (Movie) v.getTag();
            showDetailsActivity(movie);
        }
    };

    private void showDetailsActivity(Movie movie) {
        Intent movieDetailsIntent = new Intent(mContext, MovieDetailsActivity.class);
        movieDetailsIntent.putExtra(MovieDetailsActivity.EXTRA_MOVIE_ID, movie);
        movieDetailsIntent.putExtra(MovieDetailsActivity.EXTRA_COMMIT_ID, mMoviesModel.getCommitId());
        mContext.startActivity(movieDetailsIntent);
    }

    private View.OnClickListener mOnMovieImdbLinkClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Movie movie = (Movie) v.getTag();
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(movie.getImdbLink()));
            mContext.startActivity(browserIntent);
        }
    };
}