package com.mitchtodd.mercurymitch.ui;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.mitchtodd.mercurymitch.util.MovieUtil;
import com.mitchtodd.mercurymitch.R;
import com.mitchtodd.mercurymitch.data.MoviesModel;
import com.mitchtodd.mercurymitch.util.ApiUtil;
import com.mitchtodd.mercurymitch.data.api.github.model.RepoCommit;
import com.mitchtodd.mercurymitch.data.api.movies.model.Movie;
import com.mitchtodd.mercurymitch.util.PreferenceUtil;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * First, we always check for a newer commit. If newer, we invalidate and load movies, and then save
 * the model to SharedPreferences. Otherwise, we just load movies from SharedPreferences and update
 * the view.
 */
public class MainActivity extends AppCompatActivity {
    private static final String COMPANY = "MercuryIntermedia";
    private static final String REPO = "Sample_Json_Movies";
    private static final String MOVIES_MODEL_KEY = "MOVIES_MODEL_KEY";

    @InjectView(R.id.movie_list_view)
    RecyclerView mMovieListView;
    @InjectView(R.id.loading_progress)
    ProgressBar mLoadingProgressBar;
    @InjectView(R.id.no_connection_layout)
    LinearLayout mNoConnectionLayout;

    private Handler mHandler = new Handler();
    private MoviesViewState mCurrentViewState;
    private MovieListAdapter mMoviesListAdapter;
    private MoviesModel mMoviesModel = new MoviesModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        // setup movie list view and adapter
        mMovieListView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mMovieListView.setLayoutManager(layoutManager);
        mMoviesListAdapter = new MovieListAdapter(mMoviesModel);
        mMovieListView.setAdapter(mMoviesListAdapter);
        mMovieListView.setLayoutManager(new LinearLayoutManager(this));

        // always first check for newer commit
        loadLatestCommit();
    }

    /**
     * Load latest commit from GitHubService
     */
    private void loadLatestCommit() {
        setViewState(MoviesViewState.LOADING);

        ApiUtil.createGitHubService().getCompanyRepoCommits(COMPANY, REPO,
                new Callback<List<RepoCommit>>() {
                    @Override
                    public void success(List<RepoCommit> repoCommits, Response response) {
                        String latestCommit = MovieUtil.getLatestCommitSha(repoCommits);
                        checkLoadMovies(latestCommit);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        handleServiceError();
                    }
                });
    }

    /**
     * Compare latest commit from service to the commit in the saved model and updates movies if necessary
     *
     * @param latestCommit
     */
    private void checkLoadMovies(String latestCommit) {
        MoviesModel savedModel = PreferenceUtil.loadFromJson(MainActivity.this, MOVIES_MODEL_KEY, MoviesModel.class);
        if (savedModel == null || !savedModel.getCommitId().equals(latestCommit)) {
            mMoviesModel.setCommitId(latestCommit);
            mMoviesModel.getMovies().clear();
            loadMovies();
        } else {
            mMoviesModel = savedModel;
            mMoviesListAdapter.setMoviesModel(mMoviesModel);
            updateView();
        }
    }

    /**
     * Load movies from MovieService
     */
    private void loadMovies() {
        setViewState(MoviesViewState.LOADING);

        ApiUtil.createMovieService().getMovies(mMoviesModel.getCommitId(),
                new Callback<List<Movie>>() {
                    @Override
                    public void success(List<Movie> movies, Response response) {
                        mMoviesModel.getMovies().addAll(movies);
                        PreferenceUtil.saveAsJson(MainActivity.this, MOVIES_MODEL_KEY, mMoviesModel);
                        updateView();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        handleServiceError();
                    }
                });
    }

    private void handleServiceError() {
        setViewState(MoviesViewState.NO_CONNECTION);
    }

    @OnClick(R.id.no_connection_retry_button)
    public void retry() {
        loadLatestCommit();
    }

    private void updateView() {
        setViewState(MoviesViewState.SHOW_MOVIES);

        mMoviesListAdapter.notifyDataSetChanged();
    }

    /**
     * Update visibility of views depending on current state
     *
     * @param state
     */
    private void setViewState(MoviesViewState state) {
        mCurrentViewState = state;

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                switch(mCurrentViewState) {
                    case LOADING:
                        mMovieListView.setVisibility(View.INVISIBLE);
                        mNoConnectionLayout.setVisibility(View.INVISIBLE);
                        mLoadingProgressBar.setVisibility(View.VISIBLE);
                        break;
                    case NO_CONNECTION:
                        mMovieListView.setVisibility(View.INVISIBLE);
                        mNoConnectionLayout.setVisibility(View.VISIBLE);
                        mLoadingProgressBar.setVisibility(View.INVISIBLE);
                        break;
                    case SHOW_MOVIES:
                        mMovieListView.setVisibility(View.VISIBLE);
                        mNoConnectionLayout.setVisibility(View.INVISIBLE);
                        mLoadingProgressBar.setVisibility(View.INVISIBLE);
                        break;
                }
            }
        });
    }

    private enum MoviesViewState {
        LOADING,
        NO_CONNECTION,
        SHOW_MOVIES
    }
}
