package com.mitchtodd.mercurymitch.data.api.movies.model;

import java.io.Serializable;

/**
 * Class modeling movies retrieved from {@link com.mitchtodd.mercurymitch.data.api.movies.MovieService}
 */
public class Movie implements Serializable {
    private int rank;
    private String title;
    private int year;
    private String imdbId;
    private float imdbRating;
    private int imdbVotes;
    private String poster;
    private String imdbLink;

    private MovieDetails mMovieDetails = null;

    public int getRank() {
        return rank;
    }

    public String getTitle() {
        return title;
    }

    public int getYear() {
        return year;
    }

    public String getImdbId() {
        return imdbId;
    }

    public float getImdbRating() {
        return imdbRating;
    }

    public int getImdbVotes() {
        return imdbVotes;
    }

    public String getPoster() {
        return poster;
    }

    public String getImdbLink() {
        return imdbLink;
    }

    public MovieDetails getMovieDetails() {
        return mMovieDetails;
    }

    public void setMovieDetails(MovieDetails movieDetails) {
        mMovieDetails = movieDetails;
    }
}
