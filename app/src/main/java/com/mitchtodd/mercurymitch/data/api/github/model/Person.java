package com.mitchtodd.mercurymitch.data.api.github.model;

import java.util.Date;

/**
 * Class modeling person info retrieved from {@link com.mitchtodd.mercurymitch.data.api.github.GitHubService}
 */
public class Person {
    private String name;
    private String email;
    private Date date;

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public Date getDate() {
        return date;
    }
}
