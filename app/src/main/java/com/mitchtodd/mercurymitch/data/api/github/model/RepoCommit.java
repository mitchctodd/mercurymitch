package com.mitchtodd.mercurymitch.data.api.github.model;

/**
 * Class modeling repository commit info retrieved from {@link com.mitchtodd.mercurymitch.data.api.github.GitHubService}
 */
public final class RepoCommit {
    private String sha;
    private Commit commit;

    public String getSha() {
        return sha;
    }

    public Commit getCommit() {
        return commit;
    }
}
