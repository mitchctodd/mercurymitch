package com.mitchtodd.mercurymitch.data.api.github.model;

/**
 * Class modeling commit info retrieved from {@link com.mitchtodd.mercurymitch.data.api.github.GitHubService}
 */
public class Commit {
    private Person committer;

    public Person getCommitter() {
        return committer;
    }
}
