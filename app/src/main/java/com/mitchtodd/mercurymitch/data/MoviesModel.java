package com.mitchtodd.mercurymitch.data;

import com.mitchtodd.mercurymitch.data.api.movies.model.Movie;

import java.util.ArrayList;
import java.util.List;

/**
 * Root model for app. Contains list of {@link Movie}s and the commit id used to retrieve them
 * from the service.
 */
public class MoviesModel {
    private String mCommitId;
    private List<Movie> mMovies = new ArrayList<>();

    public MoviesModel() {

    }

    public String getCommitId() {
        return mCommitId;
    }

    public void setCommitId(String commitId) {
        mCommitId = commitId;
    }

    public List<Movie> getMovies() {
        return mMovies;
    }
}
