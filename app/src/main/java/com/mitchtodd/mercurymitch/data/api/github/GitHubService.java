package com.mitchtodd.mercurymitch.data.api.github;

import com.mitchtodd.mercurymitch.data.api.github.model.RepoCommit;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Path;

/**
 * API description for retrieving GitHub information.
 */
public interface GitHubService {
    String BASE_URL = "https://api.github.com";

    @Headers("Content-Type: application/json")
    @GET("/repos/{company}/{repo}/commits")
    void getCompanyRepoCommits(@Path("company") String company, @Path("repo") String repo,
                               Callback<List<RepoCommit>> cb);
}
