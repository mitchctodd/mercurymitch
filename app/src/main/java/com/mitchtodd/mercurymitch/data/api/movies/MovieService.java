package com.mitchtodd.mercurymitch.data.api.movies;

import com.mitchtodd.mercurymitch.data.api.movies.model.Movie;
import com.mitchtodd.mercurymitch.data.api.movies.model.MovieDetails;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Path;

/**
 * API description for retrieving movie information.
 */
public interface MovieService {
    String BASE_URL = "https://raw.githubusercontent.com/MercuryIntermedia/Sample_Json_Movies";

    @Headers("Content-Type: application/json")
    @GET("/{commitId}/top_movies.json")
    void getMovies(@Path("commitId") String commitId, Callback<List<Movie>> cb);

    @Headers("Content-Type: application/json")
    @GET("/{commitId}/by_id/{imdbId}.json")
    void getMovieDetails(@Path("commitId") String commitId, @Path("imdbId") String imdbId,
                         Callback<MovieDetails> cb);
}
